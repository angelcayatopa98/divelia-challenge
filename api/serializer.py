"""
Serializer class
"""
from api.models import juguetes
from django.contrib.auth import authenticate,password_validation
from django.db.models import fields
from rest_framework import serializers
from .models import Usuario,Mascota,Jueguete


class UsuarioSerializer(serializers.ModelSerializer):
    """
    Serializer for Usuario
    """
    class Meta:
        model = Usuario
        fields='__all__'

class UsuarioRegisterSerializer(serializers.ModelSerializer):

    email = serializers.EmailField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    area = serializers.CharField()

    class Meta:
        model = Usuario
        fields=('email','first_name','last_name','area')

class MascotaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mascota
        fields='__all__'

class JugueteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Jueguete
        fields = '__all__'





