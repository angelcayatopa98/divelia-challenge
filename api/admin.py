"""
Admin for models
"""
from django.contrib import admin
from .models import Usuario,Mascota,Jueguete

@admin.register(Usuario)
class UsuarioAdmin(admin.ModelAdmin):
    """
    Admin for users
    """
    list_display = ('id','email', 'nombre', 'apellido', 'area')

@admin.register(Mascota)
class UsuarioAdmin(admin.ModelAdmin):
    """
    Admin for pets
    """
    list_display = ('id','nombre', 'nombre_corto', 'imagen')

@admin.register(Jueguete)
class UsuarioAdmin(admin.ModelAdmin):
    """
    Admin for toys
    """
    list_display = ('id','nombre', 'precio', 'url')