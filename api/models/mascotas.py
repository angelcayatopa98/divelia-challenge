"""
Database model for mascota
"""
from django.db import models

class Mascota(models.Model):
    """
    Model to manage pets
    """
    duenio = models.ForeignKey('Usuario',on_delete=models.CASCADE)
    nombre = models.CharField(max_length=20)
    nombre_corto = models.CharField(max_length=10,unique=True)
    imagen = models.ImageField()

    def __str__(self):

        return self.nombre
