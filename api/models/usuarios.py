"""
Database model for usuario
"""
from django.db import models


class Usuario(models.Model):
    """
    Model to manage users
    """
    AREAS=[
        ('desarrollo','desarrollo'),
        ('disenio','disenio'),
        ('ventas','ventas'),
    ]

    nombre = models.CharField(max_length=20)
    apellido = models.CharField(max_length=20)
    email = models.EmailField(unique=True, error_messages={
                              'unique': "ya existe este correo"})
    area = models.CharField(max_length=20,choices=AREAS)

    REQUIRED_FIELDS = ['email', 'nombre', 'apellido','area']

    def __str__(self):

        return self.nombre
