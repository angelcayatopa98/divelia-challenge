"""
Database model for mascota
"""
from django.db import models

class Jueguete(models.Model):
    """
    Model to manage toys
    """
    mascota = models.ForeignKey('Mascota',on_delete=models.CASCADE)
    nombre = models.CharField(max_length=20)
    precio = models.FloatField()
    url = models.URLField(max_length=200)

    def __str__(self):

        return self.nombre