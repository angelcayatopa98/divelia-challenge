from django.urls import path,include
from django.conf.urls.static import static
from django.conf import settings


from rest_framework.routers import DefaultRouter

from .view import UsuarioViewSet,MascotaViewSet

router = DefaultRouter()
router.register(r'usuario',UsuarioViewSet,basename='usuario')
router.register(r'mascota',MascotaViewSet,basename='mascota')

urlpatterns = [
    path('',include(router.urls)),
    
]