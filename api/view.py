from api.models.juguetes import Jueguete
from rest_framework import  viewsets,mixins,status
from rest_framework.decorators import action
from rest_framework.response import Response
from .models import Usuario,Mascota
from.serializer import UsuarioSerializer,UsuarioRegisterSerializer,MascotaSerializer,JugueteSerializer


class UsuarioViewSet(mixins.CreateModelMixin,
                     mixins.ListModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset=Usuario.objects.all()

    def get_serializer_class(self):
        if self.action ==['post']:
            return UsuarioRegisterSerializer
        if self.action in ['update','partial_update','get']:
            return UsuarioRegisterSerializer
        return UsuarioSerializer
    
    @action(detail=False,methods=['post'],url_path='crear_mascota/(?P<id>[^/.]+)')
    def crear_mascota(self,request,id,pk=None):
        id=int(id)
        request.data['duenio'] = id
        serializer = MascotaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)

    @action(detail=False,methods=['put'],url_path='actualizar_mascota/(?P<id_usuario>[^/.]+)/(?P<id_mascota>[^/.]+)')
    def actualizar_mascota(self,request,id_usuario,id_mascota,pk=None):
        id_user=int(id_usuario)
        id_pet=int(id_mascota)
        request.data['duenio'] = id_user
        mascota = Mascota.objects.get(id=id_pet)
        serializer = MascotaSerializer(mascota,data=request.data,partial=False)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)

    @action(detail=False,methods=['put'],url_path='eliminar_mascota/(?P<id_mascota>[^/.]+)')
    def eliminar_mascota(self,request,id_mascota,pk=None):
        id_pet=int(id_mascota)
        mascota = Mascota.objects.get(id=id_pet)
        mascota.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=False,methods=['get'],url_path='mascota/(?P<id>[^/.]+)')
    def mascota(self,request,id,pk=None):
        id=int(id)
        request.data['duenio'] = id
        mascotas = Mascota.objects.filter(duenio=id)
        serializer = MascotaSerializer(mascotas,many=True)
        return Response(serializer.data)

class MascotaViewSet(mixins.CreateModelMixin,
                     mixins.ListModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset=Mascota.objects.all()
    @action(detail=False,methods=['post'],url_path='crear_juguete/(?P<id>[^/.]+)')
    def crear_juegute(self,request,id,pk=None):
        id=int(id)
        request.data['mascota'] = id
        juguetes_mascota = Jueguete.objects.filter(mascota=id)
        if len(juguetes_mascota) >= 3:
            return Response({"Error":"Esta mascota ya tiene 3 juguetes"})    
        serializer = JugueteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)
    